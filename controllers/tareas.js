const express = require('express');
const Tarea = require('../models/tarea');

function list(req, res, next) {


  Tarea.find()
    .then((obj) =>
      res.status(200).json({
        message: 'Listado de tareas obtenido exitosamente',
        createTarea: obj
      })
    )
    .catch((err) =>
      res.status(500).json({
        message: 'Error al obtener el listado de tareas',
        error: err,
      })
    );
}

function index(req, res, next) {
  const id = req.params.id;

  Tarea.findOne({ _id: id })
    .then((obj) =>
      res.status(200).json({
        message: 'Tarea obtenida exitosamente',
        createTarea: obj,
      })
    )
    .catch((err) =>
      res.status(500).json({
        message: 'Error al obtener la tarea',
        error: err,
      })
    );
}

function create(req, res, next) {
  const title = req.body.title;
  const status = req.body.status;
  const description = req.body.description;
  const date = req.body.date;

  let tarea = new Tarea({
    title: title,
    status: status,
    description: description,
    date: date,
  });


  tarea
    .save()
    .then((obj) =>
      res.status(200).json({
        message: 'Tarea creada exitosamente',
        obj: obj,
      })
    )
    .catch((err) =>
      res.status(500).json({
        message: 'Error al crear la tarea',
        error: err,
      })
    );
}

function replace(req, res, next) {
  const id = req.params.id;

  let title = req.body.title ? req.body.title : " ";
  let status = req.body.status ? req.body.status : " ";
  let description = req.body.description ? req.body.description : " ";
  let date = req.body.date ? req.body.date : " ";


  let tarea = {
    _title: title,
    _status: status,
    _description: description,
    _date: date,
  };

  Tarea.findOneAndUpdate({ _id: id }, tarea, { new: true })
    .then((obj) =>
      res.status(200).json({
        message: 'Tarea actualizada exitosamente',
        obj: obj,
      })
    )
    .catch((err) =>
      res.status(500).json({
        message: 'Error al actualizar la tarea',
        error: err,
      })
    );
}

function update(req, res, next) {
  const id = req.params.id;
  const title = req.body.title;
  const status = req.body.status;
  const description = req.body.description;
  const date = req.body.date;

  let tarea = {};

  if (title) {
    tarea._title = title;
  }
  if (status) {
    tarea._status = status;
  }
  if (description) {
    tarea._description = description;
  }
  if (date) {
    tarea._date = date;
  }

  Tarea.findOneAndUpdate({ _id: id }, tarea)
    .then((obj) =>
      res.status(200).json({
        message: 'Tarea actualizada exitosamente',
        obj: obj,
      })
    )
    .catch((err) =>
      res.status(500).json({
        message: 'Error al actualizar la tarea',
        error: err,
      })
    );
}

function destroy(req, res, next) {
    const id = req.params.id;
    Tarea.findOneAndDelete({ _id: id })
        .then((obj) =>
        res.status(200).json({
            message: 'Tarea eliminada exitosamente',
            obj: obj,
        })
        )
        .catch((err) =>
        res.status(500).json({
            message: 'Error al eliminar la tarea',
            error: err,
        })
        );
    }

module.exports = { list, index, create, replace, update, destroy };