const mongoose = require('mongoose');
const schema = mongoose.Schema({
    _title: String,
    _status: String,
    _description: String,
    _date: Date,
  });

class Tarea{
    constructor(title,status,description,date){
        this._title = title;
        this._status = status;
        this._description = description;
        this._date = date;
    }

    get title(){
        return this._title;
    }
    set title(title){
        this._title = title;
    }
    get status(){
        return this._status;
    }
    set status(status){
        this._status = status;
    }
    get description(){
        return this._description;
    }
    set description(description){
        this._description = description;
    }
    get date(){
        return this._date;
    }
    set date(date){
        this._date = date;
    }
}
  
schema.loadClass(Tarea);
module.exports = mongoose.model('Tarea',schema);
