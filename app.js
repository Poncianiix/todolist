const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const tareasRoutes = require('./routes/tareas');
const cors = require('cors');
// const uri ="mongodb://ec2-54-144-157-145.compute-1.amazonaws.com:27017/todolist";
mongoose.connect(process.env.MONGO);
const db = mongoose.connection;

const app = express();


db.on('open',()=>{
	console.log("Conexion correcta");
});
db.on('error',(err)=> {
	console.log("Error al conectar",err);
});

app.use(cors({
	origin: '*'
}));

app.use(bodyParser.json());
app.use(express.json());
app.use('/tareas', tareasRoutes);


/*
const port = 3000;
app.listen(port, () => console.log(`Servidor iniciado en el puerto ${port}`));

*/

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
  
	// render the error page
	res.status(err.status || 500);
	res.render('error');
  });
  
  module.exports = app;
  
  
  