# Servidor Backend con Balanceo de Cargas

En este proyecto se implentó un backend con **Express** y Mongoose como ODM para MongoDB. No obstante, se cuenta con un archivo [docker-compose.yml](docker-compose.yml) que permite la creación de dos contenedores de **Docker**, cada uno con una instancia del backend, y un contenedor de Docker con **NGINX** para el balanceo de carga entre los dos servidores web.

## Archivo app.js

En el archivo [app.js](app.js) se encuentra la configuración de la aplicación de **Express**. En este archivo se configura el puerto en el que se ejecutará la aplicación, al igual que la conexión a la base de datos de **MongoDB** por medio de una variable de entorno, que en nuestro caso está en una **instancia** de AWS.

```javascript
mongoose.connect(process.env.MONGO);
```

Además, se configuran las rutas de la aplicación, las cuales se encuentran en el directorio [routes](routes).

## Archivo Dockerfile

El archivo [Dockerfile](Dockerfile) es el archivo que se utiliza para la creación de la imagen de Docker que se utilizará para la creación de los contenedores de **Docker**. En este archivo se especifica la imagen base que se utilizará para la creación de la imagen de Docker, en nuestro caso se utilizó la imagen **node:slim**. Además, se especifica el directorio de trabajo de la imagen de Docker, el cual es **/app**, y se copian los archivos del repositorio para que pueda ejecutar el proyecto. Por último, se especifican los comandos que se ejecutarán para la creación de la imagen de Docker, en nuestro caso se instalan las **dependencias** del proyecto.

```dockerfile
FROM node:slim
WORKDIR /app
COPY . .
RUN npm install
```

Seguido de esto, se especifican una serie de variables de entorno que se utilizarán para la conexión a la base de datos de **MongoDB** e indicar el **puerto** en el que se ejecuta la instancia para luego abrirlo. Por ultimo, se indica el comando a ejecutar en la terminal para correr nuestro **backend** Estas variables de entorno se especifican en el archivo [docker-compose.yml](docker-compose.yml).

```dockerfile
ENV PORT=3000
ENV MONGO None
EXPOSE ${PORT}
CMD PORT=${PORT} node bin/www
```

## Archivo docker-compose.yml

El archivo [docker-compose.yml](docker-compose.yml) es el archivo que se utiliza para la creación de los contenedores de **Docker**. En este archivo se especifican los contenedores que se crearán, en nuestro caso se crean tres contenedores, uno para cada instancia del backend y otro para el servidor de **NGINX**. Además, se especifican las variables de entorno que se utilizarán para la conexión a la base de datos de **MongoDB** e indicar el **puerto** en el que se ejecuta la instancia para luego abrirlo. 

```dockerfile
web-app-1:
    build: .
    image: todo-list:1.0
    restart: always
    ports: 
      - 3000:3000
    environment:
      - PORT=3000
      - MONGO=mongodb://<NOMBRE_DNS_DE_IP_PRIVADA>:27017/todo-list

  web-app-2:
    build: .
    image: todo-list:1.0
    restart: always
    ports: 
      - 3001:3001
    environment:
      - PORT=3001
      - MONGO=mongodb://<NOMBRE_DNS_DE_IP_PRIVADA>:27017/todo-list
```


Para el contenedor de **NGINX** se especifica el puerto en el que se ejecutará el servidor de **NGINX** y se asigna por medio de volúmenes los archivos de configuración de **NGINX**. Asimismo se especifica que el contenedor de **Docker** que se creará dependerá de los contenedores de **Docker** de las instancias del backend.

```dockerfile
balancer:
    image: nginx
    restart: always
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
    ports: 
      - 80:80
    depends_on:
      - web-app-1
      - web-app-2
```

## Archivo nginx.conf

El archivo [nginx.conf](nginx.conf) es el archivo de configuración de **NGINX**. En este archivo se especifican los nombres de los contenedores de **Docker** que se utilizarán para el balanceo de carga al igual que el puerto en el que se ejecutan las instancias del backend.

```nginx
upstream app {
    server web-app-1:3000;
    server web-app-2:3001;
}
```

## Ejecución del proyecto

Para ejecutar el proyecto se debe ejecutar el siguiente comando en la terminal, tomando en cuenta que se tiene instalado **Docker** y **Docker Compose**, y se tiene configurado el archivo [docker-compose.yml](docker-compose.yml) con la conexión a la base de datos de **MongoDB**:

```bash
docker-compose up
```