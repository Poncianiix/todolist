FROM node:slim
WORKDIR /app
COPY . .
RUN npm install
ENV PORT=3000
ENV MONGO None
EXPOSE ${PORT}
CMD PORT=${PORT} node bin/www